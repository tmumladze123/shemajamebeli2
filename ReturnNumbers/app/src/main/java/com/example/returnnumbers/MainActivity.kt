package com.example.returnnumbers
import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.returnnumbers.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity()
{
    private val listOfUsers= mutableListOf<String>()
    private val listOfUserss= mutableListOf<EditText>(binding.editTextLastName,binding.editTextFirstName,binding.editTextAge,binding.editTextEmail)
    var activeUsers:Int=0;
    var removedUsers:Int=0;
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

    }



    fun onClickAdd(view: View)
    {
        when{

            binding.editTextFirstName.text.toString().isEmpty() -> error()
            binding.editTextLastName.text.toString().isEmpty() -> error()
            binding.editTextAge.text.toString().isEmpty() -> error()
            binding.editTextEmail.text.toString().isEmpty() -> error()
            !isValidEmail(binding.editTextEmail.text.toString()) ->error()
            binding.editTextEmail.text.toString()  in listOfUsers -> Toast.makeText(this, "User already exists", Toast.LENGTH_SHORT).show()
            else -> success()
        }
    }
    fun onClickUpdate(view: View)
    {
        Toast.makeText(this, "change is proceed with email", Toast.LENGTH_SHORT).show()
        if(binding.editTextEmail.text.toString() in listOfUsers)
        {
            var index=listOfUsers.indexOf(binding.editTextEmail.text.toString())
            for(i in index..index+3)
                listOfUsers.removeAt(i)
            listOfUsers.add( binding.editTextEmail.text.toString())
            listOfUsers.add( binding.editTextAge.text.toString())
            listOfUsers.add( binding.editTextFirstName.text.toString())
            listOfUsers.add( binding.editTextLastName.text.toString())
            Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
        }
        else
        {
            Toast.makeText(this, "User does not exits", Toast.LENGTH_SHORT).show()
        }

    }
    fun onClickRemove(view: View)
    {

        if(binding.editTextEmail.text.toString() in listOfUsers)
        {
            var index=listOfUsers.indexOf(binding.editTextEmail.text.toString())

            for(i in index..index+3)
                listOfUsers.removeAt(i)

            Toast.makeText(this, "User deleted successfully", Toast.LENGTH_SHORT).show()
            activeUsers--
            removedUsers++
            binding.textViewActive.text ="Amount of Active Users: "+activeUsers
            binding.textViewRemoved.text="Amount of Removed Users: "+removedUsers


        }
        else
        {
            Toast.makeText(this, "User does not exits", Toast.LENGTH_SHORT).show()
        }

    }
    fun isValidEmail(target: String): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    fun error()
    {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
        /*binding.editTextAge.setTextColor(Color.RED)
        binding.editTextEmail.setTextColor(Color.RED)
        binding.editTextFirstName.setTextColor(Color.RED)
        binding.editTextLastName.setTextColor(Color.RED)*/
        for(editText in listOfUserss)
            editText.setTextColor(Color.RED)
    }
    fun success()
    {
        Toast.makeText(this, "User added successfully", Toast.LENGTH_SHORT).show()
        /*binding.editTextAge.setTextColor(Color.GREEN)
        binding.editTextEmail.setTextColor(Color.GREEN)
        binding.editTextFirstName.setTextColor(Color.GREEN)
        binding.editTextLastName.setTextColor(Color.GREEN)*/
        for(editText in listOfUserss)
        {
            editText.setTextColor(Color.GREEN)
            listOfUsers.add(editText.text.toString())}

        /*listOfUsers.add( binding.editTextEmail.text.toString())
        listOfUsers.add( binding.editTextAge.text.toString())
        listOfUsers.add( binding.editTextFirstName.text.toString())
        listOfUsers.add( binding.editTextLastName.text.toString())*/
        activeUsers++
        binding.textViewActive.text ="Amount of Active Users: "+activeUsers

    }
    }

